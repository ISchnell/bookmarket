/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tncy.isc.bookmarket.services;
import net.tncy.isc.bookmarket.InventoryEntry;
import net.tncy.isc.bookmarket.bookstore;
import java.util.ArrayList;
/**
 *
 * @author student
 */
public class BookstoreService {
    
    ArrayList<bookstore> allBookstores=new ArrayList<bookstore>();
    
    public bookstore createBookstore(int id,String name)
    {
        return new bookstore(id, name);
    }
    public bookstore deleteBookstore(int id)
    {
        for (int i=0; i<allBookstores.size();i++)
        {
            if(allBookstores.get(i).getId()==id)
            {
                return allBookstores.get(i);
            }
        }
        return null;
    }

    public ArrayList<bookstore> getAllBookstores() {
        return allBookstores;
    }
    
    
}
