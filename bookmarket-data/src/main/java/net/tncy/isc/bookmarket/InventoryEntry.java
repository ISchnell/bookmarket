/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tncy.isc.bookmarket;

/**
 *
 * @author student
 */
public class InventoryEntry {
    Book book;
    int quantity;
    float averagePrice;
    float currentPrice;

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(float averagePrice) {
        this.averagePrice = averagePrice;
    }

    public float getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(float currentPrice) {
        this.currentPrice = currentPrice;
    }
    
    
    public InventoryEntry(Book book, int quantity, float averagePrice, float currentPrice)
    {
        this.book=book;
        this.quantity=quantity;
        this.averagePrice=averagePrice;
        this.currentPrice=currentPrice;
    }
}
