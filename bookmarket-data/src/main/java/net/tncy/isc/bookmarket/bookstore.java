/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tncy.isc.bookmarket;

/**
 *
 * @author student
 */
public class bookstore {
    int id;
    String name;
    InventoryEntry inventoryEntries[];

    public InventoryEntry[] getInventoryEntries() {
        return inventoryEntries;
    }

    public void setInventoryEntries(InventoryEntry[] inventoryEntries) {
        this.inventoryEntries = inventoryEntries;
    }

    public bookstore(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
