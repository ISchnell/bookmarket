/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tncy.isc.bookmarket;
/**
 *
 * @author student
 */

public class Book {
    public enum BookFormat{
    BROCHE,
    POCHE
    }
    int id;
    String title;
    String author;
    String publisher;
    BookFormat format;
    String isbn;

    public Book(int id, String title, String author, String publisher, BookFormat format, String isbn) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.format = format;
        this.isbn = isbn;
    }
}
